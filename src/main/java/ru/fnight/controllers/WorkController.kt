package ru.fnight.controllers

import javafx.scene.control.TableView
import javafx.scene.layout.AnchorPane
import ru.fnight.Dobj
import ru.fnight.Type

class WorkController {
    lateinit var types: List<Type>
    lateinit var mainPaneWork: AnchorPane

    companion object {
        lateinit var objects: List<Dobj>
    }

    fun createElements(workController: WorkController) {
        for (type in workController.types) {
            val tableView = TableView<Dobj>()
            workController.mainPaneWork.children.add(tableView)
        }
    }
}