package ru.fnight.controllers

import javafx.collections.FXCollections
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.CheckBox
import javafx.scene.control.ChoiceBox
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.stage.Stage
import ru.fnight.Type


class SettingsController {
    lateinit var typesList: ListView<String>
    lateinit var attrsList: ListView<String>
    lateinit var attrTypeCb: ChoiceBox<String>
    lateinit var isListCb: CheckBox
    lateinit var createAttrButton: Button
    lateinit var typeNameText: TextField
    lateinit var attrNameText: TextField
    lateinit var changeAttrNameButton: Button
    lateinit var changeTypeNameButton: Button
    lateinit var goGoWorkButton: Button

    companion object {
        var types = ArrayList<Type>()
        lateinit var selectedType: Type
    }

    fun createType() {
        types.add(Type("New type" + types.size))
        typesList.items = FXCollections.observableArrayList(types.map(Type::name))
    }

    fun createAttr() {
        selectedType.addAttr("New arrt" + selectedType.getAttrNames().size, Type.STRING_IDENT, false)
        attrsList.items = FXCollections.observableArrayList(selectedType.getAttrNames())
    }

    fun selectType() {
        if (typesList.selectionModel.selectedItem != null) {
            selectedType = types.find { type -> typesList.selectionModel.selectedItem == type.name }!!
            attrsList.items = FXCollections.observableArrayList(selectedType.getAttrNames())
            typeNameText.text = typesList.selectionModel.selectedItem
            attrTypeCb.selectionModel.clearSelection()
            isListCb.isSelected = false
            attrsList.selectionModel.clearSelection()
            createAttrButton.isDisable = false
            typeNameText.isDisable = false
            attrNameText.isDisable = true
            attrTypeCb.isDisable = true
            isListCb.isDisable = true
            changeAttrNameButton.isDisable = true
            changeTypeNameButton.isDisable = false
        }
    }

    fun selectAttr() {
        if (attrsList.selectionModel.selectedItem != null) {
            val availableTypes = mutableListOf<String>()
            Type.TYPES.forEach { t: String -> availableTypes.add(t) }
            types.forEach { type  -> availableTypes.add(type.name) }
            attrTypeCb.items = FXCollections.observableArrayList(availableTypes)
            attrTypeCb.selectionModel.select(selectedType.getAttrTypeByName(attrsList.selectionModel.selectedItem))
            isListCb.isSelected = selectedType.getIsListByName(attrsList.selectionModel.selectedItem)
            attrNameText.text = attrsList.selectionModel.selectedItem
            attrTypeCb.isDisable = false
            isListCb.isDisable = false
            attrNameText.isDisable = false
            changeAttrNameButton.isDisable = false
        }
    }

    fun changeAttrType() {
        selectedType.setTypeAttr(attrsList.selectionModel.selectedItem, attrTypeCb.value)
    }

    fun changeAttrIsList() {
        selectedType.setIsList(attrsList.selectionModel.selectedItem, isListCb.isSelected)
    }

    fun changeTypeName() {
        if (typesList.selectionModel.selectedItem != null) {
            val selectedType = types.find { type -> type.name == typesList.selectionModel.selectedItem } ?: throw RuntimeException("cannot find type")
            selectedType.name = typeNameText.text
            typesList.items = FXCollections.observableArrayList(types.map(Type::name))
            typesList.refresh()
            //TODO: add rename exist type attr
            typesList.selectionModel.select(typeNameText.text)
        }
    }

    fun changeAttrName() {
        if (attrsList.selectionModel.selectedItem != null) {
            val selectedType = types.find { type -> type.name == typesList.selectionModel.selectedItem } ?: throw RuntimeException("cannot find type")
            val selectedAttr = selectedType.attrs.find { attr -> attr.name == attrsList.selectionModel.selectedItem } ?: throw  RuntimeException("cannot find attr")
            selectedAttr.name = attrNameText.text
            attrsList.items = FXCollections.observableArrayList(selectedType.getAttrNames())
            attrsList.refresh()
            attrsList.selectionModel.select(attrNameText.text)
        }
    }

    fun goToWork() {
        val fxmlFile = "/fxml/work.fxml"
        val loader = FXMLLoader(javaClass.getResource(fxmlFile))
        val root = loader.load<Parent>()
        val stage = goGoWorkButton.scene.window as Stage
        stage.title = "Настройки"
        stage.scene = Scene(root)
        val workController = loader.getController<WorkController>()
        workController.types = types
        workController.createElements(workController)
        stage.show()
    }
}
