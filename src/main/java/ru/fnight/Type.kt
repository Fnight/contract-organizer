package ru.fnight

import java.util.Arrays
import kotlin.streams.toList

class Type(var name: String) {
    companion object {
        const val INT_IDENT = "int"
        const val STRING_IDENT = "string"
        val TYPES = Arrays.asList(INT_IDENT, STRING_IDENT)
    }

    var attrs = ArrayList<Attr>()

    fun addAttr(attrName: String, attrTypeIdent: String, isList: Boolean) {
        this.attrs.add(Attr(attrName, attrTypeIdent, isList))
    }

    fun getAttrNames(): List<String> {
        return this.attrs.stream().map(Attr::name).toList()
    }

    fun setTypeAttr(attrName: String, typeIdent: String) {
        val attr = attrs.find { attr -> attr.name == attrName } ?: throw RuntimeException("unknown attr name")
        attr.typeIdent = typeIdent
    }

    fun setIsList(attrName: String, isList: Boolean) {
        val attr = attrs.find { attr -> attr.name == attrName } ?: throw RuntimeException("unknown attr name")
        attr.isList = isList
    }

    fun getIsListByName(attrName: String): Boolean {
        val attr = attrs.find { attr -> attr.name == attrName } ?: throw RuntimeException("unknown attr name")
        return attr.isList
    }

    fun getAttrTypeByName(name: String): String {
        val attr = attrs.find { attr -> attr.name == name } ?: throw RuntimeException("unknown attr name")
        return attr.typeIdent
    }
}