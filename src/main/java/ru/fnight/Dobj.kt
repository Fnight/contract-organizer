package ru.fnight

class Dobj(var typeName: String) {
    var values = HashMap<String, Any>()

    operator fun set(attr: String, any: Any) {
        this.values[attr] = any
    }

    operator fun get(attr: String): Any? {
        if (this.values.containsKey(attr)) {
            return this.values[attr]
        } else {
            throw RuntimeException("unknown attr type")
        }
    }
}