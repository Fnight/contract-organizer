package ru.fnight

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import ru.fnight.controllers.SettingsController

class MainApp : Application() {
    companion object {
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            launch(MainApp::class.java)
        }
    }

    override fun start(stage: Stage) {
        val fxmlFile = "/fxml/settings.fxml"
        val loader = FXMLLoader(javaClass.getResource(fxmlFile))
        val root = loader.load<Parent>()
        stage.title = "Настройки"
        stage.scene = Scene(root)
        val settingsController = loader.getController<SettingsController>()
        settingsController.attrTypeCb.valueProperty().addListener({ _, _, new ->
            if (new != null) {
                settingsController.changeAttrType()
            }
        })
        stage.show()
    }
}